# Ubuntu mit Oracle Datenbank#

Dieses Dockerfile ist ein [trusted build](https://registry.hub.docker.com/u/blakeberg/oracledb/) im [Docker Registry](https://registry.hub.docker.com).
Als DBMS wird eine **Oracle Express Edition 11g Release 2** verwendet und als Hostsystem ein **Ubuntu 14.4.1** als offizielles Image. 
*Das DBMS ist bereits installiert.*

**Oracle OTN Lizenz:** 
[http://www.oracle.com/technetwork/licenses/database-11g-express-license-459621.html](http://www.oracle.com/technetwork/licenses/database-11g-express-license-459621.html)

Erforderlich für den Container sind mind. 5 GB freier Plattenplatz und 2 GB Arbeitsspeicher. 

## Versionen ##
Die Version ***latest*** entspricht dem ***Git-master*** und beinhaltet die laufende Entwicklung. Stabile und laufende Versionen werden jeweils getaggt:

* **1.0:** erste Version 


## Installation ##
Bitte immer die Version angeben, da sonst die aktuelle in der Entwicklung stehende Version abgerufen wird.

1. `docker pull blakeberg/oracledb:1.0`
2. `docker run -d -h orxe11g --name=orxe11g -p 49132:22 -p 49133:1521 blakeberg/oracledb:1.0`

### SSH ###

* `ssh root@localhost -p 49132`
* Passwort: admin

### JDBC ###

* host: 192.168.59.103
* port: 49133
* sid: xe
* userid: system
* password: oracle